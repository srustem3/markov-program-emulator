/**
 * @author Rustem Saitgareev and Anna Novikova
 *         11-602
 *         Эмулятор нормальных алгоритмов Маркова (использование).
 */
public class Main {
    public static void main(String[] args) {
        Realization aa = new Realization("Example Markov algorithms\\1. Aa.txt");
        Realization oranges = new Realization("Example Markov algorithms\\2. Oranges.txt");
        Realization doubler = new Realization("Example Markov algorithms\\3. Doubler.txt");
        Realization numberSystem = new Realization("Example Markov algorithms\\4. Number system.txt");
    }
}
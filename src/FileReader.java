import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Rustem Saitgareev and Anna Novikova
 *         11-602
 *         Эмулятор нормальных алгоритмов Маркова (считывание с файла).
 */
public class FileReader {
    private Scanner file;
    private String[] leftSideArray;
    private String[] rightSideArray;
    private Boolean[] isStopArray;
    private ArrayList<String> leftSide = new ArrayList<>();
    private ArrayList<String> rightSide = new ArrayList<>();
    private ArrayList<Boolean> isStop = new ArrayList<>();
    private String line;
    private String thisLine;

    public FileReader (String fileName) {
        try {
            file = new Scanner(new File(fileName));
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
            System.exit(1);
        }
        readRules();
    }

    private void readRules() {
        readNextLine();
        String[] thisRule;
        while (thisLine.contains("->") || thisLine.contains("=>")) {
            if (thisLine.contains("->")) {
                thisRule = thisLine.split("->", 2);
                isStop.add(false); // Правило, после которого алгорифм не прекращает работу
            } else {
                thisRule = thisLine.split("=>", 2);
                isStop.add(true); // Правило, после которого алгорифм прекращает работу
            }
            leftSide.add(thisRule[0].trim());
            rightSide.add(thisRule[1].trim());
            readNextLine();
        }
        try {
            readLastLine();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.exit(1);
        }
    }

    private void readNextLine() {
        try {
            thisLine = file.nextLine().trim();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.exit(1);
        }
    }

    private void readLastLine() throws Exception {
        line = thisLine;
        if (file.hasNextLine()) {
            throw new Exception("Неверный формат данных в файле (лишняя строка)");
        }
        listToArray();
    }

    private void listToArray() {
        leftSideArray = leftSide.toArray(new String[leftSide.size()]);
        rightSideArray = rightSide.toArray(new String[rightSide.size()]);
        isStopArray = isStop.toArray(new Boolean[isStop.size()]);
    }

    public String[] getLeftSideArray() {
        return leftSideArray;
    }

    public String[] getRightSideArray() {
        return rightSideArray;
    }

    public Boolean[] getIsStopArray() {
        return isStopArray;
    }

    public String getLine() {
        return line;
    }
}
/**
 * @author Rustem Saitgareev and Anna Novikova
 *         11-602
 *         Эмулятор нормальных алгоритмов Маркова (реализация).
 */
public class Realization {
    public Realization(String fileName) {
        FileReader input = new FileReader(fileName);
        String[] leftSideArray = input.getLeftSideArray();
        String[] rightSideArray = input.getRightSideArray();
        Boolean[] isStopArray = input.getIsStopArray();
        String line = input.getLine();
        final int MAX_IT = 1000; // Максимальное число выполненных замен
        boolean flag = true;
        int i = 0;
        int count = 0;
        int count_rules = leftSideArray.length;
        boolean stop = false;
        System.out.println("Было: " + line);
        while (flag && count < MAX_IT && !stop) {
            if (line.contains(leftSideArray[i])) {
                stop = isStopArray[i];
                line = line.replaceFirst(leftSideArray[i], rightSideArray[i]);
                System.out.println(Integer.toString(count + 1) + ") " + line);
                i = 0; // Возврат к первому правилу
                count++;
            }
            else {
                if (i == count_rules - 1) flag = false;
                // flag стает равным false, если проверены все правила и больше невозможно применить ни одно
                // При такой ситуации i (номер проверяемого правила) будет равен максимальному номеру
                i++; //Если текущее правило не сработало, переход к следующему
            }
        }
        if (count == MAX_IT) {
            System.out.println("Превышено максимальное количество замен - " + MAX_IT);
        } else {
            System.out.println("Cтало: " + line);
        }
    }
}